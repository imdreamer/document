# 文档管理系统

#### 介绍
私有云盘/企业网盘，云端文档管理协同系统

数百种文件格式在线预览、编辑和播放
轻松分享，高效协作，细粒度权限管控
全平台客户端覆盖，随时随地访问，轻松同步挂载

#### 软件架构
 PHP 语言，

文件存储 支持本地存储、minio、 阿里oss等

#### 软件截图
 ![输入图片说明](images/%E7%99%BB%E5%BD%95.png)


![输入图片说明](images/%E4%B8%BB%E9%A1%B5.png)


![输入图片说明](images/%E6%96%87%E4%BB%B6%E7%AE%A1%E7%90%86-%E4%B8%8A%E4%BC%A0.png)

#### 联系方式
QQ 75039960，  
微信 18665802636



